<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MELB BACKOFFICE</title>
</head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="./css/style.css">
<body>

<?php 
  
  if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $name = $_GET['name'];
    // echo "Variable 'a' is set.<br>";
  }else{
    $id = -1;
    $name = "ยังไม่ได้เลือกตู้";
  }
?>
    <div class="container-fluid bg">
      <div class="row">
        <img class="logo-melb" src="./assets/image/only_logo.png">
      </div>

      <div class="row">
      <div class="dropdown" style="margin-left:80px; margin-top:20px;">
      <label for="">กรุณาเลือกตู้ &nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp; </label>
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <?php echo $name; ?>
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <?php
        require '../open_connect.php';
        $sql = "SELECT * FROM tblvending";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
        $i = 0;
            while($row = $result->fetch_assoc()) {      
        ?>
          <a class="dropdown-item" href="?id=<?php echo $row["id"];?>&name=<?php echo $row["vendingname"];?>"><?php echo $row["vendingname"]; ?></a>
        <?php 
          }
          } 
          require '../close_connect.php';
        ?>    
        </div>
      </div>
      </div>


    <div class="row">
    <table class="table" style="margin-left:5vw;margin-right:5vw;margin-top:20px;">
      <thead class="thead-dark" >
        <tr>
          <th scope="col">#</th>
          <th scope="col">id_youtube</th>
          <th scope="col">type</th>
          <th scope="col">time</th>
          <th scope="col">status</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <!-- Button trigger modal -->
          <th>ADD</th>
          <th scope="col">

            <form action="./php/upload.php" method="post" enctype="multipart/form-data" style = "margin-bottom:10px;">
              <input type="file" name="fileToUpload" id="fileToUpload">
              <input class="btn btn-primary" type="submit" value="Upload Image" name="submit">
            </form>

            <div data-toggle="modal" data-target="#youtube" style="color:blue;"><img src="./assets/image/youtube.png" style="width:20px;height:20px; margin-right:10px;">เพิ่มรายการจาก Youtube</div>
            <br/>
            <div data-toggle="modal" data-target="#image" style="color:blue;"><img src="./assets/image/image.png" style="width:20px;height:20px; margin-right:10px;">เพิ่มรายการจากคลังภาพ</div>
            <br/>
            <div data-toggle="modal" data-target="#video" style="color:blue;"><img src="./assets/image/music.png" style="width:20px;height:20px; margin-right:10px;">เพิ่มรายการจากคลังวีดีโอ</div>
          </th>
          <!-- Modal youtube -->
          <div class="modal fade" id="youtube" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
              <form action="./php/add.php" method="POST">
                  <div class="modal-header">
                    <h5 class="modal-title">ใส่รายละเอียดของวีดีโอ</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">
                      <div class="form-group">
                        <label for="">ID Youtube</label>
                        <input type="text"
                          class="form-control" name="url"aria-describedby="helpId" placeholder="">
                        <small id="helpId" class="form-text text-muted">ใส่แค่ Id จาก Path ของ Link Youtube</small>
                      </div>
                      <div class="form-group">
                        <label for="">เวลาในการแสดง (นาที) </label>
                        <input type="text"
                          class="form-control" name="time" aria-describedby="helpId" placeholder="">
                        <small id="helpId" class="form-text text-muted">ใส่เวลาที่มีหน่วยเป็นนาที</small>
                      </div>
                      <input type="text" style="display:none;" name="group1" value="youtube"/>
                      <input type="text" style="display:none;" name="idv" value="<?php echo $id ?>"/>
                      
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                </form>
              </div>
            </div>
          </div>

          <!-- Modal image -->
          <div class="modal fade" id="image" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
              <form action="./php/add.php" method="POST">
                  <div class="modal-header">
                    <h5 class="modal-title">ใส่รายละเอียดของรูปภาพ</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                      
                      
                  <div class="modal-body">
                      <div class="form-group">
                        <label>PATH Image</label>
                        <!-- <input type="text"
                          class="form-control" name="url"aria-describedby="helpId" placeholder=""> -->
                          <select class="form-control"name="url">

                          <?php 
                            foreach(glob('../contant/assets/image_select/*.*') as $filename){
                              echo '<option value="'.str_replace("../contant/assets/","",$filename).'">'.str_replace("../contant/assets/","",$filename).'</option>';
                            }
                          ?>
                            
                            
                          </select>
                        <small id="helpId" class="form-text text-muted">เลือกรูปภาพจากรายการที่แสดง</small>
                      </div>
                      <div class="form-group">
                        <label for="">เวลาในการแสดง (นาที) </label>
                        <input type="text"
                          class="form-control" name="time" aria-describedby="helpId" placeholder="">
                        <small id="helpId" class="form-text text-muted">ใส่เวลาที่มีหน่วยเป็นนาที</small>
                      </div>
                      <input type="text" style="display:none;" name="group1" value="image"/>
                      <input type="text" style="display:none;" name="idv" value="<?php echo $id ?>"/>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                </form>
              </div>
            </div>
          </div>


          <!-- Modal video -->
          <div class="modal fade" id="video" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
              <form action="./php/add.php" method="POST">
                  <div class="modal-header">
                    <h5 class="modal-title">ใส่รายละเอียดของไฟล์วีดีโอ</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                      
                  <div class="modal-body">
                      <div class="form-group">
                        <label>PATH Video</label>
                        <!-- <input type="text"
                          class="form-control" name="url"aria-describedby="helpId" placeholder=""> -->
                          <select class="form-control"name="url">

                          <?php 
                            foreach(glob('../contant/assets/video_select/*.*') as $filename){
                              echo '<option value="'.str_replace("../contant/assets/","",$filename).'">'.str_replace("../contant/assets/","",$filename).'</option>';
                            }
                          ?>
                            
                          </select>
                        <small id="helpId" class="form-text text-muted">เลือกไฟล์วีดีโอจากรายการที่แสดง</small>
                      </div>
                      <div class="form-group">
                        <label for="">เวลาในการแสดง (นาที) </label>
                        <input type="text"
                          class="form-control" name="time" aria-describedby="helpId" placeholder="">
                        <small id="helpId" class="form-text text-muted">ใส่เวลาที่มีหน่วยเป็นนาที</small>
                      </div>
                      <input type="text" style="display:none;" name="group1" value="video"/>
                      <input type="text" style="display:none;" name="idv" value="<?php echo $id ?>"/>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                </form>
              </div>
            </div>
          </div>

        </tr>

        <?php
        require '../open_connect.php';
        $sql = "SELECT * FROM tblcontent WHERE vending_id = $id";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
        $i = 0;
            while($row = $result->fetch_assoc()) {      
        ?>
        <tr>
          <th scope="row"><?php echo $row["id"]; ?> <a href="./php/del.php?id=<?php echo $row["id"]; ?>" style="color:red;">[-]</a></th>
          <td><?php echo $row["url_emb"]; ?></td>

          <?php
            if($row["type"] == 'image'){
              echo '<td><img src="./assets/image/image.png" style="width:30px;height:30px;"></td>';
            }else if($row["type"] == 'video'){
              echo '<td><img src="./assets/image/music.png" style="width:30px;height:30px;"></td>';
            }else if($row["type"] == 'youtube'){
              echo '<td><img src="./assets/image/youtube.png" style="width:30px;height:30px;"></td>';
            } 
          ?>

          <td><?php echo $row["time"]; ?> minute</td>
          <?php
            if($row["flag_open"] == 'y'){
              echo '<td><a href="./php/status.php?id='.$row["id"].'&status=n" class="btn btn-success">กำลังทำงาน</a></td>';
            }else if($row["flag_open"] == 'n'){
              echo '<td><a href="./php/status.php?id='.$row["id"].'&status=y" class="btn btn-danger">ไม่ทำงาน</a></td>';
            } 
          ?>
        </tr>
    <?php 
    }
    } 
    require '../close_connect.php';
    ?>
    </tbody>
    </table>

        </div>
    </div>
</body>
</html>