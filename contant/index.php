<!DOCTYPE html>
<html lang="en">
<?php 
    $id = $_GET['id'];
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MELB</title>
</head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="./css/style.css">
<link rel="stylesheet" type="text/css" href="./css/btn.css">
<link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans&display=swap" rel="stylesheet">
<body>
    <div class="container-fluid">
    <?php
        session_start();
        if(isset($_SESSION['videoplayid']) && $_SESSION['videoplayid'] == 1) {
        } else if(!isset($_SESSION['videoplayid']) || (isset($_SESION['videoplayid']) && $_SESSION['videoplayid'] == 0)){
            $_SESSION['videoplayid'] = 0;
        }
        require '../open_connect.php';
        $sql = "SELECT * FROM tblcontent WHERE Flag_open = 'y' AND vending_id = $id";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
        $i = 0;
        while($row = $result->fetch_assoc()) {
            if($i == $_SESSION['videoplayid']){
        ?>
            <script>
            jQuery(function ($) {
                var fiveMinutes = <?php echo $row["time"] ?> * 60,display = $('#time');
                startTimer(fiveMinutes, display);
            });
            </script>

            <div class="row">
                <div class="col-9 no-space">   
                <?php 
                    if($row["type"] == 'youtube'){
                ?>
                    <iframe style="border: 0; width: 100%; height: 100vh; pointer-events: none;" src="https://www.youtube.com/embed/<?php echo $row["url_emb"]?>?version=3&autoplay=1&loop=1&mute=1&enablejsapi=1&autopause=0&modestbranding=1&autohide=1&showinfo=0&controls=0" frameborder="0" allow="autoplay; fullscreen;" allowfullscreen></iframe>
                <?php
                    }
                    else if($row["type"] == 'image'){
                ?>
                    <img style="border: 0; width: 100%; height: 100vh" src="./assets/<?php echo $row["url_emb"]?>" >
                <?php 
                    }
                    else if($row["type"] == 'video'){
                ?>
                    <video style="border: 0; width: 100%; height: 100vh" controls autoplay muted><source src="./assets/<?php echo $row["url_emb"]?>" type="video/mp4"></video>
                <?php 
                    }
                ?>
                </div>
    <?php 
            if($_SESSION['videoplayid'] >= $result->num_rows-1){
                $_SESSION['videoplayid'] = -1;
            }   
        }
        $i++;
        }

        if($_SESSION['videoplayid'] >= $result->num_rows-1){
            $_SESSION['videoplayid'] = -1;
        }   
        } else {
        }
        
        require '../close_connect.php';
    ?>   
            <div class="col-3 no-space">
                    <img class="bar" src="./assets/image/bg1.jpeg"> 

                    <span class="clock" id="time"></span> <span class="videoIndex"><?php echo ($_SESSION['videoplayid']+1)."/".$result->num_rows;?> </span>
                    <div class="center">
                        <img class="logo-size" src="./assets/image/mask.png">
                        <br/>
                        <p class='font' style="text-align: center; font-size:1.5vw;">จำนวนหน้ากากอนามัย</p>
                        <p id="count" class="font" style="text-align: center; font-size:4vw;">loading...</p>
                    <center><a href="../index.php?id=<?php echo $id; ?>"> <button class="glow-on-hover btn-get" type="button">กดเพื่อรับหน้ากาก</button></a></center>

                    </div>
                 
                </div>
            </div>
    <script>
        function startTimer(duration, display) {
            var timer = duration, minutes, seconds;
            setInterval(function () {
                minutes = parseInt(timer / 60, 10)
                seconds = parseInt(timer % 60, 10);

                minutes = minutes < 10 ? "0" + minutes : minutes;
                seconds = seconds < 10 ? "0" + seconds : seconds;
                display.text(minutes + ":" + seconds);

                showCount(<?php echo $id; ?>);
                if (--timer < 0) {
                    <?php $_SESSION['videoplayid']++ ?>
                    location.reload();
                }
            }, 1000);
        }

        function showCount(vid) {
            
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("count").innerHTML = this.responseText;
            }
            };
            xmlhttp.open("GET","./getcount.php?id="+vid ,true);
            xmlhttp.send();
        }
    </script>
    </div>
</body>
</html>

