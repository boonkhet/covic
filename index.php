<!DOCTYPE html>
<html lang="en">
<?php 
    $id = $_GET['id'];
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MELB</title>
</head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="./css/style.css">
<link rel="stylesheet" type="text/css" href="./css/keyboard.css">
<body>
    <div class="container-fluid bg">
        <div class="row center">
            <img class="logo-melb" src="./assets/image/only_logo.png">
            <img class="logo-size" src="./assets/image/mask.png">
            <form action="confirm.php" method="POST" style="width:100%; text-align:center;">
                
                <!-- <input name="numberid" type="text" onkeypress="return isNumberKey(event)" maxlength="10" class="form-control tel-input" type="text" placeholder="กรุณากรอกเบอร์โทร" require_once> -->
                <input class='hide' type="text" name="id" value="<?php echo $id;?>">
    
                <textarea id="write" name="tel" class="form-control tel-input" rows="1" onkeypress="return isNumberKey(event)" maxlength="10" cols="60" placeholder="กรุณากรอกเบอร์โทร" require_once></textarea>
                <ul id="keyboard" class="keyboardtop">
                    <li class="symbol"><span class="off">1</span>
                    <li class="symbol"><span class="off">2</span>
                    <li class="symbol"><span class="off">3</span>
                    <br/>
                    <li class="symbol"><span class="off">4</span>
                    <li class="symbol"><span class="off">5</span>
                    <li class="symbol"><span class="off">6</span>
                    <br/>
                    <li class="symbol"><span class="off">7</span>
                    <li class="symbol"><span class="off">8</span>
                    <li class="symbol"><span class="off">9</span>
                    <br/>
                    <li class="symbol"><span class="off"></span>
                    <li class="symbol"><span class="off">0</span>
                    <li class="delete lastitem">Delete</li>
                </ul>

                
                <br/>
                <button id="btn1" class="btn-save" type="submit">ดำเนินการต่อ</button>

            </form>
            
            <span style="position:absolute; right:30px; top:10px;" id="time"></span> <span class="videoIndex"></span>
        </div>
    </div>
    
</body>
    <script type="text/javascript" src="js/keyboard.js"></script>
    <script type="text/javascript" src="js/keyboard2.js"></script>
    <script language=Javascript>


        $("#keyboard").hide();
        $("#write").focusin(function() {
            if(screen.width >= 1281){
                $("#keyboard").show();
            }
        }).focusout(function () {
        });


      function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
         return true;
      }

            var fiveMinutes = 1 * 60,display = $('#time');
            startTimer(fiveMinutes, display);
        
        function startTimer(duration, display) {
            if(screen.width >= 1281){
                var timer = duration, minutes, seconds;
                setInterval(function () {
                    minutes = parseInt(timer / 60, 10)
                    seconds = parseInt(timer % 60, 10);

                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    seconds = seconds < 10 ? "0" + seconds : seconds;
                    display.text(minutes + ":" + seconds);

                    if (--timer < 0) {
                        window.location.href = './contant/index.php?id=<?php echo $id; ?>';
                    }
                }, 1000);
            }
        }

        
        
   </script>
</html>